package com.example.steinnxlabs.princesapeach;

import android.app.Application;

import com.example.steinnxlabs.princesapeach.Dagger.DaggerPrincesaPeachApiComponent;
import com.example.steinnxlabs.princesapeach.Dagger.PrincesaPeachApiComponent;
import com.example.steinnxlabs.princesapeach.Dagger.PrincesaPeachModulo;

/**
 * Created by SteinnxLabs on 21-06-2017.
 */
//Punto 4

//Para Poder acceder al objeto en cualquier momento de la aplicaccion.. vamos a instanciarlo
public class MainApp extends Application {
    PrincesaPeachApiComponent princesaPeachApiComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeInjector();

    }
    private void initializeInjector() {
        princesaPeachApiComponent = DaggerPrincesaPeachApiComponent.builder()
                .princesaPeachModulo(new PrincesaPeachModulo())
                .build();
    }

    public PrincesaPeachApiComponent getPrincesaPeachApiComponent() {
        return princesaPeachApiComponent;
    }
}
