package com.example.steinnxlabs.princesapeach.Dagger;

import com.example.steinnxlabs.princesapeach.PrincesaPeachActivity;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Module;

/**
 * Created by SteinnxLabs on 21-06-2017.
 */
//Punto 3

//1.-El Componente es el Puente de los Modulos previamente Creados y el Codigo que esta solicitando estos objetos
//2.-Ademas el componente es una interfaz que posee la anotacion
//   @Component en la cual se espeficican los modulos con los cuales trabajaras.. pueden mas de 1 modulo
@Singleton @Component (modules = {PrincesaPeachModulo.class})
public interface PrincesaPeachApiComponent {
    void inject(PrincesaPeachActivity princesaPeachActivity);
}
