package com.example.steinnxlabs.princesapeach.Dagger;

import com.example.steinnxlabs.princesapeach.Entidades.PrincesaPeachApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by SteinnxLabs on 21-06-2017.
 */
//Punto2
//Modulo se crean con @Modulo: Su Objetivo es Que Dagger sepa como devolvera los objetos que despues
//                              seran requeridos en nuestra app
@Module
public class PrincesaPeachModulo {

@Provides @Singleton PrincesaPeachApi providesPricensaPeach(){return new PrincesaPeachApi();}
}
