package com.example.steinnxlabs.princesapeach.Entidades;

import com.example.steinnxlabs.princesapeach.R;

/**
 * Created by SteinnxLabs on 21-06-2017.
 */

public class PrincesaPeach {
    private String nombre;
    private int foto;

    public PrincesaPeach() {
        this.nombre = "PrincesaPeach";
        this.foto = R.drawable.ic_peach;
    }

    public String getName() {
        return nombre;
    }

    public int getPhoto() {
        return foto;
    }
}
