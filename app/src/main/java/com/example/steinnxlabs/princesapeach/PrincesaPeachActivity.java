package com.example.steinnxlabs.princesapeach;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.steinnxlabs.princesapeach.Dagger.PrincesaPeachApiComponent;
import com.example.steinnxlabs.princesapeach.Entidades.PrincesaPeach;
import com.example.steinnxlabs.princesapeach.Entidades.PrincesaPeachApi;

import javax.inject.Inject;

import butterknife.BindView;

public class PrincesaPeachActivity extends BaseActivity {

    @Inject PrincesaPeachApi princesaPeachApi;
    TextView v_textView;
    ImageView v_imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_princesa_peach);
        v_textView =(TextView) findViewById(R.id.tv_nombre);
        v_imageView =(ImageView) findViewById(R.id.iv_imagen);
        //InjectarDatos
        initializeDagger();
        //Crea objeto y lo instancia
        PrincesaPeach princesaPeach = getPrincesaPeachApi();
        //Renderizar Datos
        renderizarObjeto(princesaPeach);
    }

    @Override
    protected int getLayoutResID() {
        return R.layout.activity_princesa_peach;
    }

    //Renderizar Datos
    private void renderizarObjeto(PrincesaPeach princesaPeach) {
        renderizarNombre(princesaPeach.getName());
        renderizarImagen(princesaPeach.getPhoto());
    }
    private void renderizarNombre(String name) { v_textView.setText(name);}
    private void renderizarImagen(@DrawableRes int photo) {v_imageView.setImageDrawable(ContextCompat.getDrawable(this,photo));}


    //Metodo que devuelve el objeto PrincesaPeach
    private PrincesaPeach getPrincesaPeachApi(){
        return princesaPeachApi.getPrincesaPeach();
    }
    private void initializeDagger() {
        MainApp application = (MainApp) getApplication();
        application.getPrincesaPeachApiComponent().inject(this);
    }

}
